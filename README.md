#  GitLab Client for iOS

The original repository is hosted at [`gitlab.com/felix-schindler/gitlab-ios`](https://gitlab.com/felix-schindler/gitlab-ios). Additionally, an official mirror is available on [极狐](https://jihulab.com/felix-schindler/tanuki-mirror).

## Disclaimer

This is **NOT** an official repository of GitLab Inc. You can find the original [GitLab-Repository here](https://gitlab.com/gitlab-org/gitlab).

I'm just a student who started working on this out of boredom.

## Dev

0. Clone the repo - `git clone git@gitlab.com:felix-schindler/gitlab-ios.git`
0. Open in Xcode - `cd gitlab-ios && open GitLab.xcodeproj`
0. Have fun getting it to run 👨🏻‍💻

## Links

- [App on AppStore](https://apps.apple.com/app/tanuki-for-gitlab/id6446419487)
- [Xcode](https://developer.apple.com/xcode/)
- [Swift](https://www.swift.org/)
- [SwiftUI](https://developer.apple.com/xcode/swiftui/)

## Tokei

```
===============================================================================
 Language            Files        Lines         Code     Comments       Blanks
===============================================================================
 JSON                    4           41           41            0            0
 Markdown                1           22            0           14            8
 Swift                  67         7971         6847          503          621
===============================================================================
 Total                  72         8034         6888          517          629
===============================================================================
```
