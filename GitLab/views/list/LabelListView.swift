//
//  LabelListView.swift
//  GitLab
//
//  Created by Felix Schindler on 06.11.21.
//

import SwiftUI

struct LabelListView: View {
	@State var labels: [APILabel]
	@State var showDescription = false
	@State var showEmpty = false
	
	/// Project ID (needed for deletion)
	@State var projectId: Int?
	@State var deletionError = false
	
	var body: some View {
		if (showEmpty && labels.isEmpty) {
			Text("There are no labels")
		} else {
			ForEach(labels, id: \.id) { label in
				VStack(alignment: .leading) {
					PillView(label.name.emojized(), bgColor: Color(hex: label.color), fgColor: Color(hex: label.textColor))
					if (showDescription && !(label.description?.isEmpty ?? true)) {
						Text(label.description!.emojized())
							.font(.footnote)
					}
				}.if(showDescription) { view in
					view.swipeActions {
						AsyncButton(action: {
							if (projectId != nil) {
								let code = await API.delete(endpoint: "projects/\(projectId!)/labels/\(label.id)")
								deletionError = (code.rawValue < 200 || code.rawValue >= 300)
							} else {
								deletionError = true
							}
						}, role: .destructive, label: {
							Label("Delete", systemImage: "trash")
						}).alert(isPresented: $deletionError, content: {
							Alert(title: Text("Error"), message: Text("Failed to delete label"), dismissButton: .default(Text("OK")))
						})
					}
				}
			}
		}
	}
}

struct LabelListView_Previews: PreviewProvider {
	static var previews: some View {
		List {
			HStack {
				LabelListView(labels: [
					APILabel(id: 2, name: "bug", description: "", color: "#d9534f", textColor: "#FFFFFF"),
					APILabel(id: 3, name: "documentation", description: "", color: "#0000FF", textColor: "#FFFFFF")
				], showDescription: false)
			}
			LabelListView(labels: [
				APILabel(id: 1, name: ":rocket: enhancement", description: "Something describing it", color: "#5cb85c", textColor: "#FFFFFF"),
				APILabel(id: 2, name: "bug", description: "", color: "#d9534f", textColor: "#FFFFFF"),
				APILabel(id: 3, name: "documentation", description: "", color: "#f0ad4e", textColor: "#FFFFFF")
			], showDescription: true)
		}
	}
}
