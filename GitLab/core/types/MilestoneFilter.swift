//
//  MilestoneFilter.swift
//  GitLab
//
//  Created by Felix Schindler on 26.03.23.
//

import Foundation

enum MilestoneState: String {
	case NAME  = "state"
	case active = "active",
			 closed = "closed",
			 all
}
