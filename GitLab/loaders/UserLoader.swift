//
//  UserLoader.swift
//  GitLab
//
//  Created by Felix Schindler on 20.03.23.
//

import SwiftUI

struct UserLoader: View {
	// Parameters
	/// UserID
	@State var id: Int = 0
	/// Whether to load the logged in user. When true, no ID needed
	@State var loadSelf = false
	
	@State var user: User? = nil
	@State var status: UserStatus? = nil
	@State var loadFailed = false
	
	var body: some View {
		List {
			if (user != nil && status != nil) {
				SingleUserView(user: user!, status: status!)
					.navigationTitle(user!.name.isEmpty ? user!.username : user!.name)
			} else if (loadFailed) {
				Text(Messages.failedToLoad)
			} else {
				ProgressView()
			}
		}.onAppear() {
			Task {
				await loadUserAndStatus()
			}
		}.refreshable {
			await loadUserAndStatus()
		}.toolbar {
			if let url = URL(string: user?.webUrl ?? "") {
				ShareButton(url)
			}
		}.navigationTitle("User")
	}
	
	private func loadUserAndStatus() async -> Void {
		user = await API.get(type: User.self, endpoint: loadSelf ? "user" : "users/\(id)")
		status = await API.get(type: UserStatus.self, endpoint: loadSelf ? "user/status" : "users/\(id)/status")
		loadFailed = (user == nil || status == nil)
	}
}

struct UserLoader_Previews: PreviewProvider {
	static var previews: some View {
		NavigationStack {
			UserLoader(id: 9005085)
		}
	}
}
