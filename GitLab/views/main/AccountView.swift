//
//  AccountView.swift
//  GitLab
//
//  Created by Felix Schindler on 30.10.21.
//

import SwiftUI

struct AccountView: View {
	@State var user: User? = nil
	@State var status: UserStatus? = nil
	
	@State var showInfo: Bool = false
	@State var showSettings: Bool = false
	
	var body: some View {
		NavigationStack {
			UserLoader(loadSelf: true)
				.toolbar {
					RoundIconButton("Show Info", icon: "info") {
						showInfo = true
					}
					RoundIconButton("Show Settings", icon: "gearshape") {
						showSettings = true
					}
				}.sheet(isPresented: $showSettings) {
					SettingsView()
				}.sheet(isPresented: $showInfo) {
					InfoView()
				}
		}.navigationViewStyle(StackNavigationViewStyle())
	}
}

struct AccountView_Previews: PreviewProvider {
	static var previews: some View {
		AccountView()
	}
}
