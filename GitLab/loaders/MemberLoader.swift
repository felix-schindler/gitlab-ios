//
//  ProjectMemberLoader.swift
//  GitLab
//
//  Created by Felix Schindler on 19.03.23.
//

import SwiftUI

struct MemberLoader: View {
	/// Project ID
	@State var id: Int
	/// Group ID
	@State var groupId: Int

	@State var members: [UserSmall]? = nil
	@State var loadFailed: Bool = false
	
	@State var showNewMember = false

	init(id: Int = 0, groupId: Int = 0) {
		self.id = id
		self.groupId = groupId
	}
	
	var body: some View {
		List {
			if (members != nil) {
				UserSmallListView(users: members!)
			} else if (loadFailed) {
				Text(Messages.failedToLoad)
			} else {
				ProgressView()
			}
		}.onAppear {
			Task {
				members = await getMembers()
				loadFailed = (members == nil)
			}
		}.refreshable {
			if let temp = await getMembers() {
				members = temp
			}
		}.toolbar {
            if (id != 0 || groupId != 0) {
				RoundIconButton("New Member", icon: "person.badge.plus") {
					showNewMember = true
				}
            }
		}.sheet(isPresented: $showNewMember) {
			NewMember(id: id, groupId: groupId)
		}.navigationTitle((id == 0 && groupId == 0) ? "Users" : "Members")
	}
	
	private func getMembers() async -> [UserSmall]? {
		var endpoint = ""
		if (id != 0) {
			endpoint = "projects/\(id)/members"
		} else if (groupId != 0) {
			endpoint = "groups/\(groupId)/members"
		} else {
			endpoint = "users"
		}
		return await API.get(type: [UserSmall].self, endpoint: endpoint)
	}
}

#Preview {
	NavigationStack {
		MemberLoader(id: 33025310)
	}
}
